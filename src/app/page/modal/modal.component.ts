import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalService } from 'src/app/modal.service';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
})
export class ModalComponent implements OnInit, OnDestroy {
  show: boolean;

  private subShow: Subscription;

  constructor(private modalService: ModalService) {
    this.show = this.modalService.getShow();
    this.subShow = this.modalService.showChanged.subscribe((show) => {
      this.show = show;
    });
  }

  ngOnInit() {}

  close() {
    this.modalService.setShow(false);
  }

  ngOnDestroy() {
    this.subShow.unsubscribe();
  }
}
