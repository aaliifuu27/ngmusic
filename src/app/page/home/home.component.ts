import { Component } from '@angular/core';
import { SearchService } from 'src/app/search.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  constructor(private searchService: SearchService) {
    this.searchService.setPage('home');
  }
}
