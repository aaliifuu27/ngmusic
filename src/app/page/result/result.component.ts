import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SearchService } from 'src/app/search.service';

@Component({
  selector: 'result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})
export class ResultComponent implements OnDestroy {
  songs: any[];
  keywords: string;
  loading: boolean;

  private subSongs: Subscription;
  private subKeywords: Subscription;
  private subLoading: Subscription;

  constructor(private searchService: SearchService) {
    this.searchService.setPage('result');
    this.songs = this.searchService.getSongs();
    this.keywords = this.searchService.getKeywords();
    this.loading = true;
    this.subKeywords = this.searchService.keywordsChanged.subscribe(
      (keywords) => {
        this.keywords = keywords;
      }
    );
    this.subSongs = this.searchService.songsChanged.subscribe((songs) => {
      this.songs = songs;
    });
    this.subLoading = this.searchService.loadingChanged.subscribe((loading) => {
      this.loading = loading;
    });
  }

  ngOnDestroy() {
    this.subSongs.unsubscribe();
    this.subKeywords.unsubscribe();
    this.subLoading.unsubscribe();
  }
}
