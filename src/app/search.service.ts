import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalService } from './modal.service';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private url: string;

  private page: string;
  pageChanged = new Subject<string>();

  private keywords: string;
  keywordsChanged = new Subject<string>();

  private songs: any[];
  songsChanged = new Subject<any[]>();

  private loading: boolean;
  loadingChanged = new Subject<boolean>();

  constructor(private http: HttpClient, private modalService: ModalService) {
    this.url = 'https://itunes.apple.com/search?term=';
    this.page = '';
    this.keywords = '';
    this.songs = [];
    this.loading = false;
  }

  setPage(data: any) {
    this.page = data;
    this.pageChanged.next(this.page);
  }

  setSongs(data: any) {
    this.songs = data;
    this.songsChanged.next(this.songs);
  }

  setKeywords(data: string) {
    console.log(data);
    this.keywords = data;
    this.keywordsChanged.next(this.keywords);
  }

  setLoading(data: boolean) {
    this.loading = data;
    this.loadingChanged.next(this.loading);
  }

  getPage() {
    return this.page;
  }

  getSongs() {
    return this.songs;
  }

  getKeywords() {
    return this.keywords;
  }

  getLoading() {
    return this.loading;
  }

  search(keywords: string) {
    if (keywords) {
      this.setLoading(true);
      this.setKeywords(keywords);
      this.http
        .jsonp(this.url + keywords.trim() + '&entity=song', 'callback')
        .subscribe(
          (res) => {
            let result: any;
            result = res;
            let songs: any[] = result.results || [];
            this.setSongs(songs);
            this.setLoading(false);
            this.modalService.setShow(false);
          },
          (err: HttpErrorResponse) => {
            console.log(err);
            this.setLoading(false);
            this.modalService.setShow(false);
          }
        );
    }
  }
}
