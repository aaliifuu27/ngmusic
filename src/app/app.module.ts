import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './page/home/home.component';
import { ResultComponent } from './page/result/result.component';
import { SearchComponent } from './search/search.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppbarComponent } from './appbar/appbar.component';
import { SongCardComponent } from './song-card/song-card.component';
import { ModalComponent } from './page/modal/modal.component';

import { SearchService } from './search.service';
import { ModalService } from './modal.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    HomeComponent,
    ResultComponent,
    AppbarComponent,
    SongCardComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    AppRoutingModule,
    FontAwesomeModule,
  ],
  providers: [SearchService, ModalService],
  bootstrap: [AppComponent],
})
export class AppModule {}
