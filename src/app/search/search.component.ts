import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ModalService } from '../modal.service';
import { SearchService } from '../search.service';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: [
    './search.component.scss'
  ]
})
export class SearchComponent implements OnDestroy {
  keywords: string;
  btnClass: string;
  page: string;
  loading: boolean;

  private subPage: Subscription;
  private subLoading: Subscription;
  private subSongs: Subscription;

  constructor(private searchService: SearchService, private modalService: ModalService, private router: Router) {
    this.keywords = "";
    this.page = this.searchService.getPage();
    this.loading = this.searchService.getLoading();
    console.log(this.loading)
    this.subPage = this.searchService.pageChanged.subscribe((page) => {
      this.page = page;
    });
    this.subLoading = this.searchService.loadingChanged.subscribe((loading) => {
      console.log(loading)
      this.loading = loading;
    });
    this.subSongs = this.searchService.songsChanged.subscribe((songs) => {
      if (this.page === "home") this.router.navigate(['/result']);
      this.modalService.setShow(false);
    });
    this.btnClass = this.page === "home" ? "bg-purple-400" : "from-[#712bda] to-[#a45deb] bg-gradient-to-r"
  }

  search() {
    this.searchService.search(this.keywords);
  }

  ngOnDestroy(): void {
      this.subPage.unsubscribe();
      this.subLoading.unsubscribe();
      this.subSongs.unsubscribe();
  }
}
