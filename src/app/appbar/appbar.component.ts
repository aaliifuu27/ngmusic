import { Component } from '@angular/core';
import { ModalService } from '../modal.service';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'appbar',
  templateUrl: './appbar.component.html',
})
export class AppbarComponent {
  faBars = faBars;
  faSearch = faSearch;

  constructor(private router: Router, private modalService: ModalService) {}

  show() {
    this.modalService.setShow(true);
  }
  home() {
    this.router.navigate(['']);
  }
}
