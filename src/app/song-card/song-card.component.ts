import { Component, Input } from '@angular/core';
import { faDollarSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'song-card',
  templateUrl: './song-card.component.html',
})
export class SongCardComponent {
  @Input() public data: any;
  faDollarSign = faDollarSign;

  constructor() {
    this.data = undefined;
  }
  
}
