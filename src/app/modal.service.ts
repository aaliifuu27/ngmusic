import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ModalService {

  private show: boolean;
  showChanged = new Subject<boolean>();

  constructor() {
    this.show = false;
  }

  setShow(data: boolean) {
    this.show = data;
    this.showChanged.next(this.show);
  }

  getShow() {
    return this.show;
  }

}
